Analysis Scripts for D'Alessio et al. SinoPHB project
==================

*The main files here are:

- scripts.R -> master script for generating main figures
- contributors -> the contributing authors
- RNAseq differential gene expression in mutants.xls -> the output of cuffdiff comparisons 
	between thewildtype in the two conditions and each mutant and the wildtype showing log2 
	fold change and significance
- BioSample accession numbers -> accession number and information for all 36 samples sequenced
- SRA_metadata2 -> metadata for the 36 samples in the SRA database

The folder 'data' contains all the data files needed to run the scripts in the 'scripts.R' file